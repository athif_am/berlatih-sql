CREATE TABLE users (
    id int int AUTO_INCREMENT,
    name varchar(255),
    email varchar(255),
    password varchar(255),
    PRIMARY KEY (id)
);

CREATE TABLE items(
    id int AUTO_INCREMENT,
    name varchar(255),
    description varchar(255),
    Price int,
    Stock int,
    category_id int FOREIGN KEY REFERENCES categories(category_id)
    PRIMARY KEY (id)
);

CREATE TABLE items(
    id int AUTO_INCREMENT,
    name varchar(255),
    PRIMARY KEY (id)
);